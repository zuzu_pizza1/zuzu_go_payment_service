package storage

import (
	"context"
	"market6405216/market_go_catalog_service/genproto/payment_service"
	// "market6405216/market_go_payment_service/models"
)

type StorageI interface {
	CloseDB()
	Payment() PaymentRepoI
}

type PaymentRepoI interface {
	Create(ctx context.Context, req *payment_service.CreatePayment) (resp *payment_service.PaymentPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *payment_service.PaymentPrimaryKey) (resp *payment_service.Payment, err error)
	GetAll(ctx context.Context, req *payment_service.GetListPaymentRequest) (resp *payment_service.GetListPaymentResponse, err error)
	Update(ctx context.Context, req *payment_service.UpdatePayment) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *payment_service.PaymentPrimaryKey) error
}
