package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"market6405216/market_go_catalog_service/config"
	"market6405216/market_go_catalog_service/genproto/payment_service"

	"market6405216/market_go_catalog_service/grpc/client"
	"market6405216/market_go_catalog_service/pkg/logger"
	"market6405216/market_go_catalog_service/storage"
)

type PaymentService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*payment_service.UnimplementedPaymentServiceServer
}

func NewPaymentService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *PaymentService {
	return &PaymentService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *PaymentService) Create(ctx context.Context, req *payment_service.CreatePayment) (resp *payment_service.Payment, err error) {

	i.log.Info("---CreatePayment------>", logger.Any("req", req))

	// user, err := i.services.UserService().GetById()

	pKey, err := i.strg.Payment().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreatePayment->Payment->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Payment().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyPayment->Payment->GetByID--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (c *PaymentService) GetById(ctx context.Context, req *payment_service.PaymentPrimaryKey) (resp *payment_service.Payment, err error) {

	c.log.Info("---GetPaymentByID------>", logger.Any("req", req))

	resp, err = c.strg.Payment().GetByPKey(ctx, req)
	if err != nil {
		c.log.Error("!!!GetPaymentByID->Payment->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *PaymentService) GetList(ctx context.Context, req *payment_service.GetListPaymentRequest) (resp *payment_service.GetListPaymentResponse, err error) {

	i.log.Info("---GetPayments------>", logger.Any("req", req))

	resp, err = i.strg.Payment().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetPayment->Payment->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *PaymentService) Update(ctx context.Context, req *payment_service.UpdatePayment) (resp *payment_service.Payment, err error) {

	i.log.Info("---UpdatePayment------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Payment().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdatePayment--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Payment().GetByPKey(ctx, &payment_service.PaymentPrimaryKey{PaymentId: req.PaymentId})
	if err != nil {
		i.log.Error("!!!GetPayment->Payment->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *PaymentService) Delete(ctx context.Context, req *payment_service.PaymentPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeletePayment------>", logger.Any("req", req))

	err = i.strg.Payment().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeletePayment->Payment->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
