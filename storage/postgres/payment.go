package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"market6405216/market_go_catalog_service/genproto/payment_service"
	"market6405216/market_go_catalog_service/pkg/helper"
	"market6405216/market_go_catalog_service/storage"
)

type PaymentRepo struct {
	db *pgxpool.Pool
}

func NewPaymentRepo(db *pgxpool.Pool) storage.PaymentRepoI {
	return &PaymentRepo{
		db: db,
	}
}

func (c *PaymentRepo) Create(ctx context.Context, req *payment_service.CreatePayment) (resp *payment_service.PaymentPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "payments" (
				payment_id,
				order_id,
				cash,
				payme,
				click,
				total_summ,
				updated_at
			) VALUES ($1, $2, $3, $4, $5, $6, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.OrderId,
		req.Cash,
		req.Click,
		req.Payme,
		req.TotalSumm,
	)

	if err != nil {
		return nil, err
	}

	return &payment_service.PaymentPrimaryKey{PaymentId: id.String()}, nil
}

func (c *PaymentRepo) GetByPKey(ctx context.Context, req *payment_service.PaymentPrimaryKey) (resp *payment_service.Payment, err error) {

	query := `
		SELECT
			payment_id,
			order_id,
			cash,
			payme,
			click,
			total_summ,
			created_at,
			updated_at
		FROM "payments"
		WHERE payment_id = $1
	`

	var (
		payment_id sql.NullString
		order_id   sql.NullString
		cash       sql.NullString
		payme      sql.NullString
		click      sql.NullString
		total_summ sql.NullString
		createdAt  sql.NullString
		updatedAt  sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.GetPaymentId()).Scan(
		&payment_id,
		&order_id,
		&cash,
		&payme,
		&click,
		&total_summ,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &payment_service.Payment{
		PaymentId: payment_id.String,
		OrderId:   payment_id.String,
		Cash:      cash.String,
		Payme:     payme.String,
		Click:     click.String,
		TotalSumm: total_summ.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}

	return
}

func (c *PaymentRepo) GetAll(ctx context.Context, req *payment_service.GetListPaymentRequest) (resp *payment_service.GetListPaymentResponse, err error) {

	resp = &payment_service.GetListPaymentResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			payment_id,
			order_id,
			cash,
			payme,
			click,
			total_summ,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "payments"

	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.Search) > 0 {
		filter = " AND barcode ILIKE " + "'%" + req.Search + "%'"
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var payment payment_service.Payment

		err := rows.Scan(
			&resp.Count,
			&payment.PaymentId,
			&payment.OrderId,
			&payment.Cash,
			&payment.Payme,
			&payment.Click,
			&payment.TotalSumm,
			&payment.CreatedAt,
			&payment.UpdatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Payments = append(resp.Payments, &payment)
	}

	return
}

func (c *PaymentRepo) Update(ctx context.Context, req *payment_service.UpdatePayment) (rowsAffected int64, err error) {

	query := `
			UPDATE
			    "payments"
			SET
				payment_id = $1,
				order_id =$2,
				cash =$3,
				payme = $4,
				click = $5,
				total_summ = $6,
				updated_at = now()
			WHERE
				payment_id = $1`

	result, err := c.db.Exec(ctx, query,
		req.GetPaymentId(),
		req.GetOrderId(),
		req.GetCash(),
		req.GetPayme(),
		req.GetClick(),
		req.GetTotalSumm(),
	)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *PaymentRepo) Delete(ctx context.Context, req *payment_service.PaymentPrimaryKey) error {

	query := `DELETE FROM "payments" WHERE payment_id = $1`

	// query := `UPDATE "payments" SET deleted_at = now() WHERE payment_id = $1`

	_, err := c.db.Exec(ctx, query, req.GetPaymentId())

	if err != nil {
		return err
	}

	return nil
}
