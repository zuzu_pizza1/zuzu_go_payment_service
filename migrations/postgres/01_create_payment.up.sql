


CREATE TABLE IF NOT EXISTS "payments" (
    "payment_id" UUID PRIMARY KEY,
    "order_id" VARCHAR(50),
    "cash" VARCHAR(50),
    "payme" VARCHAR(50)   NOT NULL,
    "click" VARCHAR(50)   NOT NULL,
    "total_summ" VARCHAR(50)   NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);



SELECT
	COUNT(*) OVER(),
	product_id,
	barcode,
	product_name,
	COALESCE(price, 0),
	category_id,
	TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
	TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
	COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
FROM "products"
WHERE COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')=''
AND TRUE ORDER BY created_at DESC OFFSET 0 
